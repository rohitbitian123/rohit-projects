<?php
session_start();
include "DBConnection.php";

$reqid = (int)$_POST["reqid"];
$old_phno = $_POST["old_phno"];
$new_phno = $_POST["new_phno"];
$req_date = $_POST["year"]."-".$_POST["month"]."-".$_POST["day"];
$con_given_date = $_POST["year1"]."-".$_POST["month1"]."-".$_POST["day1"];
$confirm_date = $_POST["year2"]."-".$_POST["month2"]."-".$_POST["day2"];
$trans_status = $_POST["trans_status"];
$modi_status = $_POST["modi_status"];
$can_status = $_POST["can_status"];
$con_type = $_POST["con_type"];
$action = $_POST["action"];

try
{
    if($action=="Send")
    {
        $pst = sprintf("insert into APPLICATION_RESPONSE values(%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        mysql_real_escape_string($reqid),
        mysql_real_escape_string($old_phno),
        mysql_real_escape_string($new_phno),
        mysql_real_escape_string($trans_status),
        mysql_real_escape_string($modi_status),
        mysql_real_escape_string($can_status),
        mysql_real_escape_string($req_date),
        mysql_real_escape_string($con_given_date),
        mysql_real_escape_string($confirm_date),
        mysql_real_escape_string($con_type));
        
        $i = mysql_query($pst);
        if($i == 1)
        {
            echo "<html><body bgcolor='#CCCCCC'><table align='center'><tr><td><font color='#6666FF' size='5'>Record Inserted Successfully</font></td></tr></table></body></html>";
            echo "<table align='center'><tr><td><a href='Apps_Response.php'><font color='#6666FF' size='5'>Go To Back</font></a></td></tr></table></body></html>";
        } else
        {
            echo "<html><body bgcolor='#CCCCCC'><table align='center'><tr><td><font color='#6666FF' size='5'>Record Insertion Failed</font></td></tr></table>";
            echo "<table align='center'><tr><td><a href='Apps_Response.php'><font color='#6666FF' size='5'>Go To Back</font></a></td></tr></table></body></html>";
        }
    }
    else
    if($action=="Update")
    {
        $i = mysql_query("update APPLICATION_RESPONSE set  REQ_ID=".$reqid.",OLD_NUMBER='".$old_phno."',NEW_NUMBER='".$new_phno."',TRANSFER_STATUS='".$trans_status."',MODIFICATION_STATUS='".$modi_status."',CANCELLATION_STATUS='".$can_status."',APP_REQUEST_DATE='".$req_date."',CON_GIVEN_DATE='".$con_given_date."',CONFIRMATION_DATE='".$confirm_date."',CONNTYPE='".$con_type."' where REQ_ID=".$reqid);
        if($i == 1)
        {
            echo "<html><body bgcolor='#CCCCCC'><table align='center'><tr><td><font color='#6666FF' size='5'>Record Updated Successfully</font></td></tr></table></body></html>";
            echo "<table align='center'><tr><td><a href='Apps_Response.php'><font color='#6666FF' size='5'>Go To Back</font></a></td></tr></table></body></html>";
        } else
        {
            echo "<html><body bgcolor='#CCCCCC'><table align='center'><tr><td><font color='#6666FF' size='5'>Record Updation Failed</font></td></tr></table>";
            echo "<table align='center'><tr><td><a href='Apps_Response.php'><font color='#6666FF' size='5'>Go To Back</font></a></td></tr></table></body></html>";
        }
    }
    else
    if($action=="Delete")
    {
        $i = mysql_query("delete from APPLICATION_RESPONSE  where REQ_ID=".$reqid);
        if($i == 1)
        {
            echo "<html><body bgcolor='#CCCCCC'><table align='center'><tr><td><font color='#6666FF' size='5'>Record Deleted Successfully</font></td></tr></table></body></html>";
            echo "<table align='center'><tr><td><a href='Apps_Response.php'><font color='#6666FF' size='5'>Go To Back</font></a></td></tr></table></body></html>";
        } else
        {
            echo "<html><body bgcolor='#CCCCCC'><table align='center'><tr><td><font color='#6666FF' size='5'>Record Deletion Failed</font></td></tr></table>";
            echo "<table align='center'><tr><td><a href='Apps_Response.php'><font color='#6666FF' size='5'>Go To Back</font></a></td></tr></table></body></html>";
        }
    }
    else
    {
        echo "<html><body bgcolor='#CCCCCC'><table align='center'><tr><td><font color='#6666FF' size='5'>There is a Problem in Transaction</font></td></tr></table>";
        echo "<table align='center'><tr><td><a href='Apps_Response.php'><font color='#6666FF' size='5'>Go To Back</font></a></td></tr></table></body></html>";
    }
}
catch(exception $e)
{
    echo $e->getMessage();
}