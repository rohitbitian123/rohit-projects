-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 08:22 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `telecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `application_response`
--

CREATE TABLE IF NOT EXISTS `application_response` (
  `req_id` int(11) DEFAULT NULL,
  `old_number` varchar(25) NOT NULL,
  `new_number` varchar(25) NOT NULL,
  `transfer_status` varchar(25) DEFAULT NULL,
  `modification_status` varchar(25) DEFAULT NULL,
  `cancellation_status` varchar(25) DEFAULT NULL,
  `app_request_date` varchar(15) DEFAULT NULL,
  `con_given_date` varchar(15) DEFAULT NULL,
  `confirmation_date` varchar(15) DEFAULT NULL,
  `conntype` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billdetails`
--

CREATE TABLE IF NOT EXISTS `billdetails` (
  `req_id` int(11) DEFAULT NULL,
  `bill_no` int(11) NOT NULL,
  `phoneno` varchar(20) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `sendingdate` varchar(15) DEFAULT NULL,
  `lastdatewithoutfine` varchar(15) DEFAULT NULL,
  `conclosedate` varchar(15) DEFAULT NULL,
  `conntype` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `changednumbers`
--

CREATE TABLE IF NOT EXISTS `changednumbers` (
  `old_no` int(15) NOT NULL,
  `new_no` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `changednumbers`
--

INSERT INTO `changednumbers` (`old_no`, `new_no`) VALUES
(1234567, 21234567);

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE IF NOT EXISTS `complaints` (
  `complaint_id` int(11) NOT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `customer_name` varchar(25) DEFAULT NULL,
  `comdate` varchar(15) DEFAULT NULL,
  `conntype` varchar(15) DEFAULT NULL,
  `phoneno` varchar(25) NOT NULL,
  `message` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `complaints_solution`
--

CREATE TABLE IF NOT EXISTS `complaints_solution` (
  `complaint_id` int(11) DEFAULT NULL,
  `customer_name` varchar(25) DEFAULT NULL,
  `phone_no` varchar(25) DEFAULT NULL,
  `complaint_given_date` varchar(15) DEFAULT NULL,
  `solution_sending_date` varchar(15) DEFAULT NULL,
  `solution` varchar(100) DEFAULT NULL,
  `conntype` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `conconfirm_details`
--

CREATE TABLE IF NOT EXISTS `conconfirm_details` (
  `REQID` int(11) NOT NULL,
  `CNAME` varchar(20) DEFAULT NULL,
  `CONTYPE` varchar(15) DEFAULT NULL,
  `CONCONFIRMDATE` varchar(15) DEFAULT NULL,
  `CONREQDATE` varchar(15) DEFAULT NULL,
  `PHONENO` varchar(20) DEFAULT NULL,
  `STATUS` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demand_draft_details`
--

CREATE TABLE IF NOT EXISTS `demand_draft_details` (
  `dd_id` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `branch` varchar(30) DEFAULT NULL,
  `dd_date` varchar(12) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `draft_no` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `new_connections_details`
--

CREATE TABLE IF NOT EXISTS `new_connections_details` (
  `req_id` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL,
  `repassword` varchar(10) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `purpose` varchar(15) DEFAULT NULL,
  `facility` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `new_dddetails`
--

CREATE TABLE IF NOT EXISTS `new_dddetails` (
  `req_id` int(11) DEFAULT NULL,
  `bank_id` varchar(20) DEFAULT NULL,
  `branch` varchar(45) DEFAULT NULL,
  `dateofissue` date DEFAULT NULL,
  `dateofconn` date DEFAULT NULL,
  `amt` int(11) DEFAULT NULL,
  `ddno` int(11) DEFAULT NULL,
  `typeofconn` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `new_dddtails`
--

CREATE TABLE IF NOT EXISTS `new_dddtails` (
  `req_id` int(11) NOT NULL,
  `bank_id` varchar(20) NOT NULL,
  `branch` varchar(45) NOT NULL,
  `dateofissue` date NOT NULL,
  `dateofconn` date NOT NULL,
  `amt` int(11) NOT NULL,
  `ddno` int(11) NOT NULL,
  `typeofconn` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `phone_cancellation`
--

CREATE TABLE IF NOT EXISTS `phone_cancellation` (
  `req_id` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `reqdate` varchar(15) DEFAULT NULL,
  `typeofconn` varchar(15) DEFAULT NULL,
  `phoneno` varchar(25) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `phone_transfer_request`
--

CREATE TABLE IF NOT EXISTS `phone_transfer_request` (
  `req_id` int(11) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `phoneno` varchar(20) NOT NULL,
  `present_address` varchar(100) DEFAULT NULL,
  `new_address` varchar(100) DEFAULT NULL,
  `bankname` varchar(15) DEFAULT NULL,
  `branchncity` varchar(20) DEFAULT NULL,
  `ddissdate` varchar(15) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `ddno` int(11) DEFAULT NULL,
  `typeofconn` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purpose_details`
--

CREATE TABLE IF NOT EXISTS `purpose_details` (
  `pid` int(11) DEFAULT NULL,
  `name` varchar(15) DEFAULT NULL,
  `ptype` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `telecomlogin`
--

CREATE TABLE IF NOT EXISTS `telecomlogin` (
  `USERID` varchar(25) NOT NULL,
  `PASSWORD` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `telecomlogin`
--

INSERT INTO `telecomlogin` (`USERID`, `PASSWORD`) VALUES
('manager_m@teleservice.com', 'manager');

-- --------------------------------------------------------

--
-- Table structure for table `telephone_modi_details`
--

CREATE TABLE IF NOT EXISTS `telephone_modi_details` (
  `req_id` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `reqdate` varchar(15) DEFAULT NULL,
  `phoneno` varchar(25) NOT NULL,
  `bankname` varchar(15) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `ddno` int(11) DEFAULT NULL,
  `ddissdate` varchar(15) DEFAULT NULL,
  `branchncity` varchar(30) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_reg`
--

CREATE TABLE IF NOT EXISTS `user_reg` (
  `userid` varchar(10) NOT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `PASSWORD` varchar(10) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `dateofbirth` varchar(10) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `martialstatus` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_reg`
--

INSERT INTO `user_reg` (`userid`, `firstname`, `lastname`, `PASSWORD`, `age`, `dateofbirth`, `gender`, `martialstatus`, `email`, `address`, `city`, `state`, `country`, `zipcode`, `phone`) VALUES
('1', 'B.Praveen', 'Kumar', 'harryharry', 32, '1985-05-09', 'Male', 'Married', 'praveen.lamp@gmail.com', 'Hno : 12-2-822/A6, Venkanna Nagar Colony, Methodist Church Lane, Mehidpatnam', 'Mehdipatnam', 'Andhra Pradesh', 'INDIA', '500028', 2147483647);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application_response`
--
ALTER TABLE `application_response`
 ADD KEY `in_req_id` (`req_id`);

--
-- Indexes for table `billdetails`
--
ALTER TABLE `billdetails`
 ADD PRIMARY KEY (`bill_no`), ADD KEY `in_req_id` (`req_id`);

--
-- Indexes for table `changednumbers`
--
ALTER TABLE `changednumbers`
 ADD PRIMARY KEY (`old_no`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
 ADD PRIMARY KEY (`complaint_id`);

--
-- Indexes for table `complaints_solution`
--
ALTER TABLE `complaints_solution`
 ADD KEY `complaint_id` (`complaint_id`);

--
-- Indexes for table `demand_draft_details`
--
ALTER TABLE `demand_draft_details`
 ADD KEY `in_acc_type_code` (`dd_id`);

--
-- Indexes for table `new_connections_details`
--
ALTER TABLE `new_connections_details`
 ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `new_dddetails`
--
ALTER TABLE `new_dddetails`
 ADD KEY `req_id` (`req_id`);

--
-- Indexes for table `phone_cancellation`
--
ALTER TABLE `phone_cancellation`
 ADD KEY `in_acc_type_code` (`req_id`);

--
-- Indexes for table `phone_transfer_request`
--
ALTER TABLE `phone_transfer_request`
 ADD PRIMARY KEY (`phoneno`), ADD KEY `in_acc_type_code` (`req_id`);

--
-- Indexes for table `purpose_details`
--
ALTER TABLE `purpose_details`
 ADD KEY `in_acc_type_code` (`pid`);

--
-- Indexes for table `telecomlogin`
--
ALTER TABLE `telecomlogin`
 ADD PRIMARY KEY (`USERID`);

--
-- Indexes for table `telephone_modi_details`
--
ALTER TABLE `telephone_modi_details`
 ADD PRIMARY KEY (`phoneno`), ADD KEY `req_id` (`req_id`);

--
-- Indexes for table `user_reg`
--
ALTER TABLE `user_reg`
 ADD PRIMARY KEY (`userid`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `application_response`
--
ALTER TABLE `application_response`
ADD CONSTRAINT `application_response_ibfk_1` FOREIGN KEY (`req_id`) REFERENCES `new_connections_details` (`req_id`);

--
-- Constraints for table `billdetails`
--
ALTER TABLE `billdetails`
ADD CONSTRAINT `billdetails_ibfk_1` FOREIGN KEY (`req_id`) REFERENCES `new_connections_details` (`req_id`);

--
-- Constraints for table `complaints_solution`
--
ALTER TABLE `complaints_solution`
ADD CONSTRAINT `complaints_solution_ibfk_1` FOREIGN KEY (`complaint_id`) REFERENCES `complaints` (`complaint_id`);

--
-- Constraints for table `demand_draft_details`
--
ALTER TABLE `demand_draft_details`
ADD CONSTRAINT `demand_draft_details_ibfk_1` FOREIGN KEY (`dd_id`) REFERENCES `new_connections_details` (`req_id`);

--
-- Constraints for table `new_dddetails`
--
ALTER TABLE `new_dddetails`
ADD CONSTRAINT `new_dddetails_ibfk_1` FOREIGN KEY (`req_id`) REFERENCES `new_connections_details` (`req_id`);

--
-- Constraints for table `phone_cancellation`
--
ALTER TABLE `phone_cancellation`
ADD CONSTRAINT `phone_cancellation_ibfk_1` FOREIGN KEY (`req_id`) REFERENCES `new_connections_details` (`req_id`);

--
-- Constraints for table `phone_transfer_request`
--
ALTER TABLE `phone_transfer_request`
ADD CONSTRAINT `phone_transfer_request_ibfk_1` FOREIGN KEY (`req_id`) REFERENCES `new_connections_details` (`req_id`);

--
-- Constraints for table `purpose_details`
--
ALTER TABLE `purpose_details`
ADD CONSTRAINT `purpose_details_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `new_connections_details` (`req_id`);

--
-- Constraints for table `telephone_modi_details`
--
ALTER TABLE `telephone_modi_details`
ADD CONSTRAINT `telephone_modi_details_ibfk_1` FOREIGN KEY (`req_id`) REFERENCES `new_connections_details` (`req_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
