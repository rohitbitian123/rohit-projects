<?php
session_start();
include "DBConnection.php";

$req_id = (int)$_POST["req_id"];
$action = $_POST["action"];
$rid1 = (int)$_SESSION["USERID"];
try
{
    if($req_id == $rid1)
    {
        $rs = mysql_query("select * from APPLICATION_RESPONSE where req_id=".$req_id);
    }
    else
    {
        echo "<p align=center><font color='blue' size='4'>This is not your ReqID</font></p>";
        echo "<p align=center><font color='blue' size='4'>Enter your Correct ReqID</font></p>";
        echo "<p align=center><a href='ApplicationEnquiries.php'><font size='4'>Go Back to App Enqiry Form</font></a></p>";
    }
    while($row=mysql_fetch_array($rs))
    {
        $old_phno = $row[1];
        $new_phno = $row[2];
        $trans_status = $row[3];
        $modi_status = $row[4];
        $can_status = $row[5];
        $req_date = $row[6];
        $con_given_date = $row[7];
        $confirm_date = $row[8];
        $s = $row[9];
    }
    if($action=="GetPhTransReport")
    {
        if($trans_status=="Completed")
        {
            echo "<p align=center> <font color='#CC3300' size='5'><strong>Phone Transfer Application Response Details </strong></font></p>";
            echo "<p align=center> <font color='#66CC00'><strong>Application Status: &nbsp;<font color='magenta' size='4'> ".$trans_status."</font> </strong></font></p>";
            echo "<p align=center> Your Application Accepted and  Connection is Transfered to New Address.</p>";
            echo "<p align=center> Verify Your Connection Details. </p>";
            echo "<p align=center>&nbsp; </p>";
            echo "<p align=center><a href='EnquiryForPhoneTransfer.php'><font color='blue' size='4'>Go Back</font></a></p>";
        }
        else
        {
            echo "<p align=center> <font color='#CC3300' size='5'><strong>Phone Transfer Application Response Details </strong></font></p>";
            echo "<p align=center> <font color='#66CC00'><strong>Application Status: &nbsp;<font color='magenta' size='4'> ".$trans_status."</font> </strong></font></p>";
            echo "<p align=center> Your Application has been Verified and  it is in Processing.</p>";
            echo "<p align=center> Please wait for the Result till the ending Date. </p>";
            echo "<p align=center>&nbsp; </p>";
            echo "<p align=center><a href='EnquiryForPhoneTransfer.php'><font color='blue' size='4'>Go Back</font></a></p>";
        }
    }
    else
    if($action=="GetPhModiReport")
    {
        if($modi_status=="Completed")
        {
            echo "<p align=center> <font color='#CC3300' size='5'><strong>Phone Modification Application Response Details </strong></font></p>";
            echo "<p align=center> <font color='#66CC00'><strong>Application Status: &nbsp;<font color='magenta' size='4'>".$modi_status." </font></strong></font></p>";
            echo "<p align=center> Your Application Accepted and  Connection is Modified.</p>";
            echo "<p align=center>  Your old  Phone No:&nbsp;&nbsp;".$old_phno." </p>";
            echo "<p align=center>  Your new  Phone No:&nbsp;&nbsp;".$new_phno." </p>";
            echo "<p align=center>&nbsp; </p>";
            echo "<p align=center><a href='EnquiryForConnectionModification.php'><font color='blue'size='4'>Go Back</font></a></p>";
        }
        else
        {
            echo "<p align=center> <font color='#CC3300' size='5'><strong>Phone Modification Application Response Details </strong></font></p>";
            echo "<p align=center> <font color='#66CC00'><strong>Application Status: &nbsp; <font color='magenta' size='4'>".$modi_status."</font> </strong></font></p>";
            echo "<p align=center><font size='3'> Your Application has been Verified and  it is in Processing.</font></p>";
            echo "<p align=center><font size='3'> Please wait for the Result till the ending Date.</font> </p>";
            echo "<p align=center>&nbsp; </p>";
            echo "<p align=center><a href='EnquiryForConnectionModification.php'><font color='blue'size='4'>Go Back</font></a></p>";
        }
    } else
    if($action=="GetPhCansReport")
    {
        if($modi_status=="Completed")
        {
            echo "<p align=center> <font color='#CC3300' size='5'><strong>Connection Cancellation Application Response Details </strong></font></p>";
            echo "<p align=center> <font color='#66CC00'><strong>Application Status: &nbsp;<font color='magenta' size='4'> ".$can_status."</font></strong></font></p>";
            echo "<p align=center> <font size='3'>Your Application Accepted and  Connection is Cancelled.</font></p>";
            echo "<p align=center> <font size='3'> Your Cancelled Phone No:&nbsp;&nbsp;".$old_phno." </font></p>";
            echo "<p align=center>&nbsp; </p>";
            echo "<p align=center><a href='EnquiryForConnectionCancellation.php'><font size='4' color='blue'>Go Back</font></a></p>";
        }
        else
        {
            echo "<p align=center> <font color='#CC3300' size='5'><strong>Connection Cancellation Application Response Details </strong></font></p>";
            echo "<p align=center> <font color='#66CC00'><strong>Application Status: &nbsp; <font color='magenta' size='4'>".$can_status."</font> </strong></font></p>";
            echo "<p align=center> <font size='3'>Your Application has been Verified and  it is in Processing.</font></p>";
            echo "<p align=center> <font size='3'>Please wait for the Result till the ending Date.</font> </p>";
            echo "<p align=center>&nbsp; </p>";
            echo "<p align=center><a href='EnquiryForConnectionCancellation.php'><font size='4' color='blue'>Go Back</font></a></p>";
        }
    }
    else
    {
        echo "<p align=center> <font color='#CC3300' size='5'><strong>There is a Problem in Transaction</strong></font></p>";
        echo "<p align=center>&nbsp; </p>";
        echo "<p align=center><a href='ApplicationEnquiries.php'>Go Back</a></p>";
    }
}
catch(exception $e)
{
    echo $e->getMessage();
}
